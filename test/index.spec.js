/*global describe, it, jasmine, expect*/

import { dredge } from '../src/index';

describe('dredge', () => {
    let renderer, factory, actionA, actionB, actionC;

    beforeEach(() => {
        renderer = jasmine.createSpy('renderer');
        factory = jasmine.createSpy('factory').and.returnValue(renderer);
        actionA = jasmine.createSpy('actionA').and.returnValue({x: 1});
        actionB = jasmine.createSpy('actionB').and.returnValue({x: 2});
        actionC = jasmine.createSpy('actionC').and.returnValue({y: 3});
    });

    describe('dredge(factory)', () => {
        it('provides a dispatch function to the factory', () => {
            const dispatch = dredge(factory);
            expect(factory).toHaveBeenCalledWith(dispatch);
        });
    });

    describe('dispatch(action)', () => {
        it('runs the action providing the current state', () => {
            const dispatch = dredge(factory);
            dispatch(actionA);
            dispatch(actionB);

            expect(actionA).toHaveBeenCalledWith({});
            expect(actionB).toHaveBeenCalledWith({x: 1});
        });

        it('updates the state with the result of the action', () => {
            const dispatch = dredge(factory);
            dispatch(actionA);
            dispatch(actionB);

            expect(renderer).toHaveBeenCalledWith({x: 1});
            expect(renderer).toHaveBeenCalledWith({x: 2});
        });

        it('runs the action providing the current state when called from the factory', () => {
            const dispatch = dredge(dispatch => {
                dispatch(actionA);
                dispatch(actionB);
                return renderer;
            });

            expect(actionA).toHaveBeenCalledWith({});
            expect(actionB).toHaveBeenCalledWith({x: 1});
        });

        it('updates the state with the result of the action when called from the factory', () => {
            dredge(dispatch => {
                dispatch(actionA);
                dispatch(actionB);
                return renderer;
            });

            expect(renderer).toHaveBeenCalledWith({x: 1});
            expect(renderer).toHaveBeenCalledWith({x: 2});
        });

        it('combines the partial states returned by the actions', () => {
            const dispatch = dredge(factory);
            dispatch(actionA);
            dispatch(actionB);
            dispatch(actionC);
            expect(renderer).toHaveBeenCalledWith({x: 2, y: 3});
        });
    });
});
