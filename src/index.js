import {stream, reduce, map} from 'event-creek';

export function dredge(factory) {
    const buffer = [];
    let feed = action => buffer.push(action);

    const dispatch = message => feed(message);
    const process = factory(dispatch);
    const cycle = (state, action) => Object.assign({}, state, action(state));

    feed = stream(reduce(cycle, {}), map(process));
    buffer.forEach(action => dispatch(action));

    return dispatch;
}
