import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';

export default {
    input: 'src/index.js',
    output: {
        file: 'lib/dredge.umd.js',
        format: 'umd',
        name: 'dredge'
    },
    plugins: [
        resolve(),
        babel({
            presets: ['es2015-rollup'],
            plugins: ['external-helpers'],
            babelrc: false
        })
    ]
};
